package com.devskiller.tasks.blog.service;

import java.time.LocalDateTime;
import java.util.*;

import com.devskiller.tasks.blog.model.Post;
import com.devskiller.tasks.blog.model.dto.PostDto;
import com.devskiller.tasks.blog.repository.PostRepository;
import org.springframework.stereotype.Service;

import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.model.dto.NewCommentDto;

@Service
public class CommentService {

	private final PostService postService;

	private final PostRepository postRepository;

	private static Long commentId = 0L;

	public CommentService (PostService postService, PostRepository postRepository) {
		this.postService = postService;
		this.postRepository = postRepository;
	}

	/**
	 * Returns a list of all comments for a blog post with passed id.
	 *
	 * @param postId id of the post
	 * @return list of comments sorted by creation date descending - most recent first
	 */
	public List<CommentDto> getCommentsForPost(Long postId) {

		if(postId == null) throw new IllegalArgumentException("Post does not exist");
		PostDto pd = postService.getPost(postId);
		if (pd == null) throw new IllegalArgumentException("Post does not exist");

		pd.comments().sort(new Comparator<CommentDto>() {
			public int compare(CommentDto c1, CommentDto c2) {
				return c1.creationDate().compareTo(c2.creationDate());
			}
		});

		return pd.comments();
	}

	/**
	 * Creates a new comment
	 *
	 * @param postId id of the post
	 * @param newCommentDto data of new comment
	 * @return id of the created comment
	 *
	 * @throws IllegalArgumentException if postId is null or there is no blog post for passed postId
	 */
	public Long addComment(Long postId, NewCommentDto newCommentDto) {

		if(postId == null) throw new IllegalArgumentException("Post does not exist");
		Post p = postRepository.findById(postId).orElse(null);
		if (p == null) throw new IllegalArgumentException("Post does not exist");

		List<CommentDto> listComment = p.getComments();

		if(listComment == null) listComment = new ArrayList<>();

		listComment.add(new CommentDto(++commentId,
			newCommentDto.content(),
			newCommentDto.author(),
			null));

		p.setComment(listComment);

		postRepository.save(p);
		return commentId;
	}
}
