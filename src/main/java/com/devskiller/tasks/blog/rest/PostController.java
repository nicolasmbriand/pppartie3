package com.devskiller.tasks.blog.rest;

import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.model.dto.NewCommentDto;
import com.devskiller.tasks.blog.service.CommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import com.devskiller.tasks.blog.model.dto.PostDto;
import com.devskiller.tasks.blog.service.PostService;

import java.util.List;

@Controller
@RestController
@RequestMapping("/posts")
public class PostController {

	private final PostService postService;

	private final CommentService commentService;


	public PostController(PostService postService, CommentService commentService) {

		this.postService = postService;
		this.commentService = commentService;
	}

	@GetMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PostDto getPost(@PathVariable Long id) {
		return postService.getPost(id);
	}


	@PostMapping("/{id}/comments")
	@ResponseStatus(HttpStatus.CREATED)
	public void postComment(@PathVariable Long id, @RequestBody NewCommentDto ncd) {
		Assert.state(StringUtils.isNotBlank(ncd.content()), "Comment must not be null");
		commentService.addComment(id, ncd);
	}

	@GetMapping("/{id}/comments")
	@ResponseStatus(HttpStatus.OK)
	public List<CommentDto> getComment(@PathVariable Long id) {
		Assert.state(id != 0, "Id must not be null");
		return commentService.getCommentsForPost(id);
	}
}
