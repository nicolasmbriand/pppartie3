package com.devskiller.tasks.blog.model.dto;

import java.time.LocalDateTime;
import java.util.List;

public record PostDto(String title, String content, LocalDateTime creationDate, List<CommentDto> comments) {
}
