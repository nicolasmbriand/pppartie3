package com.devskiller.tasks.blog.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandlingAdvice {

	@ExceptionHandler
	public ProblemDetail handle (IllegalArgumentException e) {
		ProblemDetail problemDetail = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST.value());
		problemDetail.setDetail(e.getMessage());
		return problemDetail;
	}
}
